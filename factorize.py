#!/Library/Frameworks/Python.framework/Versions/3.5/bin/python3.5
##!/usr/bin/python3.5
import sys
import time
import mpmath
import mysql.connector
from mpmath import *
from zeros import *
from primes import *
import pi
import e
from pi import *
from e import *
from itertools import combinations
import gmpy2
from zeros1 import *

#tolerance = float(sys.argv[2])

def _index_(z, cursor):
   q = "SELECT val FROM zeros1 where id="+str(z)
   cursor.execute(q)
   rows = cursor.fetchall()
   if len(rows) > 0 and len(rows[0]) > 0:
         return float(rows[0][0])
   else:
         print("Value out of range. Please adjust tolerance.")
         sys.exit(1)
         return -1

def search(x, dd, cursor):
   cd = 1000
   cnt = 1
   cnt2 = 1
   while cd > dd:
      z = _index_(cnt, cursor)
      if z < 0:
          break
      dz = z + x
      cnt2 = cnt+ 1
      z2 = _index_(cnt2, cursor)
      if z2 < 0:
          break
      delta = abs(z2-z)
      _delta = delta
      while _delta  < x:
          cnt2 = cnt2 + 1
          z2 = _index_(cnt2, cursor)
          if z2 < 0:
             break
          delta = _delta
          _delta = abs(z2-z)
      if z2 < 0:
         break
      cd = abs(delta-x)
      if cd <=dd:
          break
      cnt = cnt + 1
   return cnt, cnt2
 
def calculateTolerance(v, p):
   _v = []
   cnt = 0
   l = len(str(v))
   while cnt < (l-1):
       vk = int(v[cnt])
       vk2 = int(v[cnt+1])
       cn =  vk*10 + vk2 
       pk = int(p[cn])
       if pk == 0:
          _v.append(.001)
       else:
          _v.append("." + str(pk))
       cnt = cnt + 1
   return _v

def factorize(v, cursor, p):
   global tolerance
   s = str(v)
   sz = gmpy2.mpz(s)
   r = gmpy2.f_mod(sz, 7)
   rep = 1
   if int(r) == 0:
         pass
   else:
         while r != gmpy2.mpz(0):
             s = s + str(v)
             rep = rep + 1
             sz = gmpy2.mpz(s)
             r = gmpy2.f_mod(sz, 7)
   l = len(s)
   if gmpy2.get_context().precision < l:
        gmpy2.get_context().precision = l + 5
   _s = s
   s = gmpy2.mpfr("." + str(s)) 
   #print(s)
   #print("rep: "+ str(rep))
   l = int(len(str(v)) / 2) + 1 
   cnt2 = 0
   cnt = 1
   h = 0
   eigenvals = []
   while (h < l):
      s = gmpy2.mul(s, 2)
      b = gmpy2.sub(s, 1)
      if float(b)  > 0 and cnt % rep == 0:
         h = h + 1
         #print("Iteration : "+ str(cnt2 + 1) + " , " + "cnt: " + str(cnt) + " :: "+ str(s))
         ratio = gmpy2.div(gmpy2.mpfr(s), gmpy2.mpfr(b))
         rs = str(ratio)
         eigenvals.append(str(rs))
      if float(b) > 0:
         s = s - 1
      cnt = cnt + 1
   #print(eigenvals)
   tolerance = calculateTolerance(v, p) 
   zlist = list(zip(eigenvals, tolerance))
   factor = []
   for x in zlist:
      tolerance = float(x[1])
      _x = float(x[0])
      p1, p2 = search(float(_x), tolerance, cursor)
      factor.append(int((str(abs(p1-p2)))))
   #print(factor)
   return factor

def differentiate(f1, f2):
    cnt = 0
    l = len(f1)
    results = []
    while cnt  < (l-1):
       cn1 = f1[cnt]*10 + f1[cnt+1]
       cn2 = f2[cnt]*10 + f2[cnt+1]
       sum1 = (cn1 - cn2)
       results.append(sum1)
       cnt = cnt + 1
    return results

def process(df):
    cnt = 0
    l = len(df)
    ra = []
    while cnt < l:
        res = []
        su1 = df[cnt]
        if su1 >= 0:
            res.append(su1)
        cnt2 = cnt + 1
        while cnt2 < l:
            su1 = su1 + df[cnt2]
            if su1 >= 0:
                res.append(su1)
            cnt2 = cnt2 + 1
        if len(res) == 0:
           ra.append([0])
        else:
           ra.append(res)
        cnt = cnt + 1
    return ra 

def do_magic(dd):
    if len(dd) == 0:
       return [ [0, 0] ]
    cnt = 0
    l = len(dd)
    cnt2 = 0
    res = []
    while cnt < l:
       dl = dd[cnt]
       su1 = 0
       cnt2 = 0
       stale = False
       for x in dl:
           cnt2 = cnt2 + 1
           su1 = su1 + x
           if su1 in primes:
               stale = True
               res.append([ cnt2 ])
               #res.append([ su1, cnt2 ])
               break
           if su1 in zeros:
               stale = True
               res.append([ cnt2 ])
               #res.append([ su1, cnt2 ])
               break
           if stale == False:
              res.append([0])
              stale = True
       cnt = cnt + 1
    return res

if __name__ == "__main__":
    gmpy2.get_context().precision = 1024
    num = str(sys.argv[1])
    cnx = mysql.connector.connect(user='root', password='root',  database='intfact')
    cursor = cnx.cursor()
    _pi = pi[:1024]
    _e = e[:1024]
    factorL1 = factorize(num, cursor, _pi)
    rnum = str(num)[::-1]
    #print("factorL1: " + str(factorL1))
    factorL2 = factorize(rnum, cursor, _e)
    #print("factorL2: " + str(factorL2))
    diff_factors = differentiate(factorL1, factorL2)
    #print(diff_factors)
    dd = process(diff_factors)
    #print(dd)
    magic_res = do_magic(dd)
    print(magic_res)
    l = len(num)
    print(pi[:l])
    print(e[:l][::-1])
    cursor.close()
    cnx.close()
